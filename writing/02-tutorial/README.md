# Database Normalization

In this tutorial, we will be walking through the process of converting a database table to conform to the different Normal Form (NF) requirements. So what is 'normalization'? Normalization is the process of deconstructing a table down to only the required attributes, in order to reduce redundancy. There are many different levels of normalization (1-6NF and more); this tutorial will cover 1-3NF. Beyond that is advanced, and not standard practice.

Before digging in to examples, let's go over the some of the benefits & drawbacks to normalization, so we have an idea of the end goal we are trying to achieve with each form.

**Benefits:**

* Minimize duplicate data
* Improve data integrity
* Faster inserts, updates, & deletions

**Drawbacks:**

*  Less streamlined, due to table joins
*  Slower read times, due to less efficient indexing  
  
  
We will be transforming the following UNF (unnormalized) table to 3NF:

| Name              | Address            | Grade     | Classes            | GPA | Honors |
|-------------------|--------------------|-----------|--------------------|-----|--------|
| James Denver      | 123 Pine Street    | Freshman  | CSCI 112, CSCI 132 | 3.0 | TRUE   |
| Katherine Miller  | 722 Hallow Ave.    | Senior    | CSCI 440, CSCI 447 | 3.1 | TRUE   |
| Amber Smith       | 222 Dusty Rd.      | Sophomore | CSCI 215, CSCI 232 | 2.2 | FALSE  |
| Amber Smith       | 180 Durston Street | Junior    | CSCI 305, CSCI 338 | 2.6 | FALSE  |
| Amber Smith       | 180 Durston Street | Junior    | CSCI 305, CSCI 338 | 2.6 | FALSE  |

## 1NF
---
#####Requirements:

* No multi-valued columns
* Each tuple must be unique
* Each tuple must have a primary key

#####Example:

To comply with the uniqueness constraint, we deleted the duplicate tuple of "Amber Smith". For the primary key constraint, a composite key is referenced by "Name" and "Address". Lastly, to not having any multi-valued columns, we created a 2nd table that broke apart each value for the "Classes" attribute. In order to reference this table more easily, another attribute "ID" was created.

![1NF](./images/1nf_student.png "1NF Student")  
  
![1NF](./images/1nf_classes.png "1NF Classes")  
  
  
An alternative to fixing the multi-valued column issue, is by creating a new tuple for each of the values in the multi-valued column. A picture example of this solution is provided below:  
  
![1NF](./images/1nf_alternative.png "1NF Alternative")

Both are valid solutions, but the second example suffers from added redundancy, due to data duplication of unnecessary attributes each time.

## 2NF
---
#####Requirements:

* Must be in 1NF
* The primary key must not be composite

A common thing when converting tables to different normal forms is that they will already satisfy the requirements of other forms. In the first example provided in the 1NF section, the table was incidentally converted to 2NF as well. Originally, the primary key was a composite key of "Name" and "Address". When "ID" was introduced to solve the multi-valued column constraint, it also solved 2NF requirement of primary key not being composite. 

![2NF](./images/1nf_student.png "2NF Student")  
  
![2NF](./images/1nf_classes.png "2NF Classes")  

## 3NF
---
#####Requirements:

* Must be in 2NF
* No transitive dependencies amongst attributes

The 2NF tables violate 3NF due to the "Honors" attribute being dependent on the value of the "GPA" attribute. This transitive dependency can cause integrity issues with "Honors" whenever "GPA" is modified. An example of this is if the "GPA" is updated to a new value, but the "Honors" attribute was not updated as well. This would result in the "Honors" attribute potentially being incorrect. In order to fix this issue, a new table is created that can be used as a lookup for the Student table's "GPA".

![3NF](./images/3nf_student.png "3NF Student")  
  
![3NF](./images/1nf_classes.png "3NF Classes") ![3NF](./images/3nf_grades.png "3NF GPA") 

## Additional Examples
---
1: Why are foreign keys helpful for 2NF?  
  
2: Convert the following table from 1NF to 2NF: 
  
![1NF](./images/examples_2.png "Example 2")  
  
3: Convert the following table from 2NF to 3NF: 
  
![2NF](./images/examples_3.png "Example 3")  
  

## Example Solutions
---
1: They allow you to create a second table that can be indexed with the original table's primary key. This allows you to reduce redundancy by not having to duplicate tuples in the original table, when dealing with multi-valued attributes.  
  
2:  
  
![1NF](./images/examples_2sol.png "Example 2")  
  
3:  
  
![2NF](./images/examples_3sol1.png "Example 3: Solution")  
  
![2NF](./images/examples_3sol2.png "Example 3: Solution")  

##  Extended Reading:
---
* General overview: https://www.guru99.com/database-normalization.html
* Forms beyond 3NF: https://en.wikipedia.org/wiki/Database_normalization
* Step-by-step conversion: https://www.sqa.org.uk/e-learning/MDBS01CD/page_26.htm

## Project References:
---
* https://www.guru99.com/database-normalization.html